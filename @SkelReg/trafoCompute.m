function obj = trafoCompute(obj, trafoType, cp_idx)
%TRAFOCOMPUTE Computes the specified transformation(s) based on the matched
%control points stored in the cp.points attribute
%   INPUT:  trafoType (optional): str
%               Defines the type of transformation to be computed.
%               Allowed types are 'at' and 'at_ft'. Note that a freeform
%               transformation cannot be computed without computing an
%               affine transformation first
%               (Default: 'at')
%           cp_idx: control point indices: logical array
%               The linear indices of the control points to be used for the
%               transformation. The default is all the control points. If
%               specific axons are set to be ignored, their cps would be
%               ignored using this index set

% Author: Florian Drawitsch <florian.drawitsch@brain.mpg.de>
%         Ali Karimi <ali.karimi@brain.mpg.de>

% add option to select specific control points
if ~exist('trafoType', 'var') || isempty(trafoType)
    trafoType = 'at';
else
    SkelReg.assertTrafoType(trafoType);
end

% Setting the default cp_idx
if ~exist('cp_idx', 'var') || isempty(cp_idx)
    cp_idx = true(height(obj.cp.points.matched), 1);
    if ~isempty(obj.cp.ignoreAxons.name)
        % Get the idx of axons to ignore
        switch obj.cp.ignoreAxons.searchMode
            case 'exact'
                cp_idx = ...
                    ~strcmp(obj.cp.points.matched.id, ...
                    obj.cp.ignoreAxons.name);
            case 'regexp'
                cp_idx = ~cellfun...
                    (@(x) ~isempty(regexpi(x, obj.cp.ignoreAxons.name, 'once')), ...
                    obj.cp.points.matched.id);
        end
    end
end

% Perform the transformation and save it in the corresponding property
switch trafoType
    case 'at'
        obj.assertModalityAvailability('moving', {'cp', 'points'});
        obj.affine = obj.affine.compute( ...
            obj.cp.points.matched.xyz_moving(cp_idx,:), ...
            obj.cp.points.matched.xyz_fixed(cp_idx,:), ...
            obj.cp.points.moving.Properties.UserData.scale, ...
            obj.cp.points.fixed.Properties.UserData.scale, ...
            obj.cp.points.moving.Properties.UserData.dataset, ...
            obj.cp.points.fixed.Properties.UserData.dataset);
        
    case 'at_ft'
        obj.assertModalityAvailability('moving_at', {'cp', 'points'});
        obj.freeform = obj.freeform.compute( ...
            obj.cp.points.matched.xyz_moving_at(cp_idx,:), ...
            obj.cp.points.matched.xyz_fixed(cp_idx,:), ...
            obj.cp.points.moving_at.Properties.UserData.scale, ...
            obj.cp.points.fixed.Properties.UserData.scale, ...
            obj.cp.points.moving_at.Properties.UserData.dataset,...
            obj.cp.points.fixed.Properties.UserData.dataset);
end

end

